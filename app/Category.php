<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    const CATEGORY_ACTIVE = 1;
    const CATEGORY_INACTIVE = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'slug', 'status'
    ];
}
