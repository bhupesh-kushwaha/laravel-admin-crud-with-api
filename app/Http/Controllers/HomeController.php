<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if ( \Auth::user()->email == 'admin@admin.com' ) {
                $users = User::orderBy('id','desc')->paginate(5);

                return view('admin.user',compact('users'));
        }

        return view('/home');
    }
}
