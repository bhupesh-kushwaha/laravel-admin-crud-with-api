<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Interfaces\ApiInterfaces;
use App\Http\Traits\HelperTrait;
use App\User;
use App\Product;
use App\Category;

class ApiController extends Controller implements ApiInterfaces
{
    use HelperTrait;

    /**
     * The request implementation.
     *
     * @var request
     */
    protected $request;

    /**
     * Get all caltegory details.
     *
     * @param  Category  $category
     * @return mixed
     */
    public function __construct(Request $request) {
        $this->request = $request;
    }

    /**
     * Get all category details.
     *
     * @return array
     */
    public function showCategory() {
        $categoryData = Category::where('status', Category::CATEGORY_ACTIVE)->orderBy('id', 'desc')->get();

        return $this->generateResponse(200, 'success', 'category data',
            $this->fetchResourceData('category', $categoryData)
        );
    }

    /**
     * Filter product and fetch product details.
     *
     * @param int $category
     * @return mixed
     */
    public function filterProductByCategory($category) 
    {
        $product = Product::where([
            'category_id' => $category,
            'status' => Product::PRODUCT_ACTIVE
        ])->get();

        if( $product ) {
            $productData = $product->load(['category']);

            return $this->generateResponse(200, 'success', 'product data',
                $this->fetchResourceData('product-collection', $productData)
            );
        }

        return $this->generateResponse(404, 'fail', 'Product not found');
    }

    /**
     * Search products and fetch product deatils.
     *
     * @param string $contain
     * @return mixed
     */
    public function searchProduct($contain) 
    {
        $product = Product::where('status', Product::PRODUCT_ACTIVE)
            ->where('title', 'LIKE', '%'.$contain.'%') 
            ->orWhere('description', 'LIKE', '%'.$contain.'%') 
            ->get();  

        if( $product ) {
            $productData = $product->load(['category']);

            return $this->generateResponse(200, 'success', 'product data',
                $this->fetchResourceData('product-collection', $productData)
            );
        }

        return $this->generateResponse(404, 'fail', 'Product not found');
    }

    /**
     * Get a product details by id
     *
     * @param Product $product
     * @return mixed
     */
    public function getProductByID(Product $product)
    {
        return $this->generateResponse(200, 'success', 'product data',
            $this->fetchResourceData('product', $product->load('category'))
        );
    }
}
