<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use Redirect,Response;
use App\Http\Traits\HelperTrait;
use App\Http\Requests\ProductRequest;
use App\Category;

class ProductController extends Controller
{
    use HelperTrait;
    
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('id','desc')->paginate(5);

        $categories = Category::orderBy('id','desc')->pluck('id', 'title');
   
        return view('admin.product', compact('products', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $featuredImageName = $request->featured_image_old;

        if ($request->hasFile('featured_image')) {
            $image = $request->file('featured_image');
            $featuredImageName = rand(). time() . '.' . $image->getClientOriginalName();
            $image->move( public_path() . "/products/", $featuredImageName);
        }

        $products   =   Product::updateOrCreate(
                    ['id' => $request->category_id],
                    [
                        'category_id' => $request->category_id,
                        'title' => $request->title, 
                        'slug' => $this->slugGenerator($request->title),
                        'description' => $request->description,
                        'status' => $request->status,
                        'featured_image' => $featuredImageName
                    ]
                );
        // 'uploaded_image' => '<img src="/images/'.$new_name.'" class="img-thumbnail" width="300" />',        
    
        return Response::json($products);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $products  = Product::where(['id' => $id])->first();
 
        return Response::json($products);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $products = Product::where('id',$id)->latest()->first();

        $image_path = public_path()."/uploads/".$products->getOriginal('featured_image');

        if(\File::exists($image_path)) {
            \File::delete($image_path);
        }

        $products->delete();
   
        return Response::json($products);
    }
}

