<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Category;
use Redirect,Response;
use App\Http\Traits\HelperTrait;
use App\Http\Requests\CategoryRequest;

class CategoryController extends Controller
{
    use HelperTrait;
    
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::orderBy('id','desc')->paginate(5);
   
        return view('admin.category', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $category   =   Category::updateOrCreate(
                    ['id' => $request->category_id],
                    [
                        'title' => $request->title, 
                        'slug' => $this->slugGenerator($request->title),
                        'status' => $request->status,
                    ]
                );
    
        return Response::json($category);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category  = Category::where(['id' => $id])->first();
 
        return Response::json($category);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::where('id',$id)->delete();
   
        return Response::json($category);
    }
}

