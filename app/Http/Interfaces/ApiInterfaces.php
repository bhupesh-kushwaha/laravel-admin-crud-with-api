<?php 

namespace App\Http\Interfaces;

interface ApiInterfaces {

    /**
     * Get all category details.
     *
     * @return array
     */
    public function showCategory();

    /**
     * Filter product and fetch product details.
     *
     * @param int $category
     * @return mixed
     */
    public function filterProductByCategory($category);

    /**
     * Search products and fetch product deatils.
     *
     * @param string $contain
     * @return mixed
     */
    public function searchProduct($contain);

    /**
     * Get a product details by id
     *
     * @param Product $product
     * @return mixed
     */
    public function getProductByID(\App\Product $product);
}