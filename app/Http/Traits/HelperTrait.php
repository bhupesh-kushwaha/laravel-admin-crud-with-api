<?php

namespace App\Http\Traits;

use Illuminate\Support\Facades\Response;
use App\Http\Resources\ProductResource;
use App\Http\Resources\CategoryResource;
use App\Product;
use App\Category;

trait HelperTrait 
{
     /**
     * Generate slug from given title.
     *
     * @return mixed
     */
    public function slugGenerator($title)
    {
        return str_slug($title);
    }

    /**
     * Generate Json response with given parameters.
     *
     * @return mixed
     */
    public function generateResponse(
        $status_code = 200, 
        $status_message = '', 
        $message = '',  
        array $data = null,
        array $error = null
    ) {
        return Response::json([
            'status' => $status_message,
            'message' => $message,
            'data' => $data,
            'error' => $error,
        ], $status_code);
    }

    /**
     * Get any resource data using that type.
     *
     * @param string $type
     * @param object $object
     * @return array
     */
    public function fetchResourceData($type, $object) {
        switch ($type) {
            case 'product':
                $productResource = new ProductResource( $object );

                $data = $this->generateArrayFromJsonResource($productResource);  
                
                break;

            case 'product-collection':
                $productResource = ProductResource::collection( $object );

                $data = $this->generateArrayFromJsonResource($productResource);  
                
                break;    
            
            case 'category':
                $categoryResource = CategoryResource::collection( $object );

                $data = $this->generateArrayFromJsonResource($categoryResource);   
                
                break;     
            
            default:
                $data = [];

                break;
        }

        return $data;
    }
    
    /**
     * Generate array from given json resource object.
     *
     * @param object $object
     * @return array
     */
    public function generateArrayFromJsonResource($object)
    {
        $array = $object->response()->getData(true);

        return isset($array['data']) ? $array['data'] : [];  
    }
}