<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    const PRODUCT_ACTIVE = 1;
    const PRODUCT_INACTIVE = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id', 'title', 'slug', 'featured_image', 'description', 'status'
    ];

    /**
     * Get the Image full Path.
     *
     * @return string
     */
    public function getFeaturedImageAttribute($value)
    {
        return url('/') . "/products/" . $value;
    }
    /**
     * Get the galley releted with the product.
     */
    public function productGallery()
    {
        return $this->hasMany(ProductGallery::class);
    }

    /**
     * Get the category releted with the product.
     */
    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }
}
