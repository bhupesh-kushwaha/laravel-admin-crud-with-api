<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => 'Admin Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('1234567890'),
            'phone_no' => '123456',
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);
    }
}
