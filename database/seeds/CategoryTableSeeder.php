<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        \App\Category::insert([
            'title' => $faker->catchPhrase,
            'slug' => $faker->slug,
            'status' => 1,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);
    }
}
