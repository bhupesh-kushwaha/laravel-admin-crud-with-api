<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        $product = \App\Product::create([
            'category_id' => 1,
            'title' => $faker->catchPhrase,
            'slug' => $faker->slug,
            'featured_image' => $faker->md5 . ".jpg" ,
            'description' => $faker->sentence,
            'status' => 1,
            'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        \App\ProductGallery::create([
                'product_id' => 1,
                'images' => $faker->md5 . ".jpg",
                'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);

        \App\ProductGallery::create([
                'product_id' => 1,
                'images' => $faker->md5 . ".jpg",
                'created_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);
    }
}
