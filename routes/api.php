<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'V1', 'namespace' => 'Api\V1'], function () {

    Route::post('login', 'ApiUserController@login');

    Route::post('register', 'ApiUserController@register');
});

Route::group([
    'prefix' => 'V1', 
    'namespace' => 'Api\V1',
    'middleware' => 'auth:api'
], function () {

    Route::get( 'category/show', 'ApiController@showCategory' )->name('category.show');
    
    Route::get( 'product/{category}/show', 'ApiController@filterProductByCategory' )->name('product.category');

    Route::get( 'product/{contain}/search', 'ApiController@searchProduct' )->name('product.search');

    Route::get( 'product/{product}/view', 'ApiController@getProductByID' )->name('product.view');
});
