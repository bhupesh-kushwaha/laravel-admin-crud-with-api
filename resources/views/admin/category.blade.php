@extends('layouts.app')

@section('content')
<div class="container">
    
    <div class="row">
        <div class="col-12">
          <a href="javascript:void(0)" class="btn btn-success mb-2" id="create-new-category">Add category</a> 
          
          <table class="table table-bordered" id="category_crud">
           <thead>
              <tr>
                 <th>Id</th>
                 <th>Title</th>
                 <th>Slug</th>
                 <th>Status</th>
                 <td colspan="2">Action</td>
              </tr>
           </thead>
           <tbody id="categories-crud">
              @foreach($categories as $category)
              <tr id="category_id_{{ $category->id }}">
                 <td>{{ $category->id  }}</td>
                 <td>{{ $category->title }}</td>
                 <td>{{ $category->slug }}</td>
                 <td>{{ $category->status === 1 ? "Active" : "Inactive" }}</td>
                 <td><a href="javascript:void(0)" id="edit-category" data-id="{{ $category->id }}" class="btn btn-info">Edit</a></td>
                 <td>
                  <a href="javascript:void(0)" id="delete-category" data-id="{{ $category->id }}" class="btn btn-danger delete-category">Delete</a></td>
              </tr>
              @endforeach
           </tbody>
          </table>
          {{ $categories->links() }}
       </div> 
    </div>
</div>
<div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="categoryCrudModal"></h4>
    </div>
    <div class="modal-body">
        <form id="categoryForm" name="categoryForm" class="form-horizontal">
           <input type="hidden" name="category_id" id="category_id">
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="title" name="title" value="" required="">
                </div>
            </div>
 
            <div class="form-group">
                <label class="col-sm-2 control-label">Slug</label>
                <div class="col-sm-12">
                    <input class="form-control" id="slug" name="slug" value="" readonly disabled>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Status</label>
                <div class="col-sm-12">
                    <select class="form-control" id="status" name="status">
                        <option value="">-- Select --</option>
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
                    </select>
                </div>
            </div>

            <div class="col-sm-offset-2 col-sm-10">
             <button type="button" class="btn btn-primary" id="btn-save" value="create-category">Save
             </button>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        
    </div>
</div>
</div>
</div>
@endsection

@push('scripts')
<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#create-new-category').click(function () {
            $('#btn-save').val("create-category");
            $('#categoryForm').trigger("reset");
            $('#categoryCrudModal').html("Add New category");
            $('#ajax-crud-modal').modal('show');
        });

        $('body').on('click', '#edit-category', function () {
            var category_id = $(this).data('id');
            $.get('category/'+category_id+'/edit', function (data) {
                $('#categoryCrudModal').html("Edit category");
                $('#btn-save').val("edit-category");
                $('#ajax-crud-modal').modal('show');
                $('#category_id').val(data.id);
                $('#title').val(data.title);
                $('#slug').val(data.slug);  
                $('#status').val(data.status);  
            })
        });

        $('body').on('click', '.delete-category', function () {
            var category_id = $(this).data("id");
            if(confirm("Are You sure want to delete !")) {
                $.ajax({
                    type: "DELETE",
                    url: "{{ url('category')}}"+'/'+category_id,
                    success: function (data) {
                        $("#category_id_" + category_id).remove();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        });

        $('body').on('click', '#btn-save', function(e) {
            e.preventDefault();

            var actionType = $('#btn-save').val();
            $('#btn-save').html('Sending..');
            $.ajax({
                data: $('#categoryForm').serialize(),
                url: "{{ route('category.store') }}",
                type: "post",
                dataType: 'json',
                success: function (data) {
                    var category = '<tr id="category_id_' + data.id + '"><td>' + data.id + '</td><td>' + data.title + '</td><td>' + data.slug + '</td>' + '</td><td>' + (data.status === 0 ? "Active" : "Inactive") + '</td>';
                    category += '<td><a href="javascript:void(0)" id="edit-category" data-id="' + data.id + '" class="btn btn-info">Edit</a></td>';
                    category += '<td><a href="javascript:void(0)" id="delete-category" data-id="' + data.id + '" class="btn btn-danger delete-category">Delete</a></td></tr>';
                    
                    
                    if (actionType == "create-category") {
                        $('#categories-crud').prepend(category);
                    } else {
                        $("#category_id_" + data.id).replaceWith(category);
                    }

                    $('#categoryForm').trigger("reset");
                    $('#ajax-crud-modal').modal('hide');
                    $('#btn-save').html('Save Changes');
                    
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#btn-save').html('Save Changes');
                }
            });
        });

    });    
</script>
@endpush