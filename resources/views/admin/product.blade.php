@extends('layouts.app')

@section('content')
<div class="container">
    
    <div class="row">
        <div class="col-12">
          <a href="javascript:void(0)" class="btn btn-success mb-2" id="create-new-product">Add product</a> 
          
          <table class="table table-bordered" id="product_crud">
           <thead>
              <tr>
                 <th>Id</th>
                 <th width="10%">Image</th>
                 <th>Title</th>
                 <th>Slug</th>
                 <th>Description</th>
                 <th>Status</th>
                 <td colspan="2">Action</td>
              </tr>
           </thead>
           <tbody id="products-crud">
              @foreach($products as $product)
              <tr id="product_id_{{ $product->id }}">
                 <td>{{ $product->id  }}</td>
                 <td><img width="50" height="50" src="{{ $product->featured_image }}" alt="{{ $product->title }}"/></td>
                 <td>{{ $product->title }}</td>
                 <td>{{ $product->slug }}</td>
                 <td>{{ $product->description }}</td>
                 <td>{{ $product->status === 1 ? "Active" : "Inactive" }}</td>
                 <td><a href="javascript:void(0)" id="edit-product" data-id="{{ $product->id }}" class="btn btn-info">Edit</a></td>
                 <td>
                  <a href="javascript:void(0)" id="delete-product" data-id="{{ $product->id }}" class="btn btn-danger delete-product">Delete</a></td>
              </tr>
              @endforeach
           </tbody>
          </table>
          {{ $products->links() }}
       </div> 
    </div>
</div>
<div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
<div class="modal-dialog modal-lg">
<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="productCrudModal"></h4>
    </div>
    <div class="modal-body">
        <form id="productForm" name="productForm" class="form-horizontal" enctype="multipart/form-data">
           <input type="hidden" name="product_id" id="product_id">

           <div class="form-group">
                <label class="col-sm-2 control-label">Category</label>
                <div class="col-sm-12">
                    <select class="form-control" id="category_id" name="category_id">
                        <option value="">-- Select --</option>
                        @foreach ($categories as $key => $item)
                            
                            <option value="{{$item}}">{{$key}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Title</label>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="title" name="title" value="" required="">
                </div>
            </div>
 
            <div class="form-group">
                <label class="col-sm-2 control-label">Slug</label>
                <div class="col-sm-12">
                    <input class="form-control" id="slug" name="slug" value="" readonly disabled>
                </div>
            </div>

            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Description</label>
                <div class="col-sm-12">
                    <textarea class="form-control" id="description" name="description" required=""></textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">Featured Image</label>
                <div class="col-sm-12">
                    <input type="file" class="form-control" id="featured_image" name="featured_image" required>
                    <input type="hidden" name="featured_image_old" id="featured_image_old" />
                </div>
            </div>
            

            <div class="form-group">
                <label class="col-sm-2 control-label">Status</label>
                <div class="col-sm-12">
                    <select class="form-control" id="status" name="status">
                        <option value="">-- Select --</option>
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
                    </select>
                </div>
            </div>

            <div class="col-sm-offset-2 col-sm-10">
             <button type="button" class="btn btn-primary" id="btn-save" value="create-product">Save
             </button>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        
    </div>
</div>
</div>
</div>
@endsection

@push('scripts')
<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#create-new-product').click(function () {
            $('#btn-save').val("create-product");
            $('#productForm').trigger("reset");
            $('#productCrudModal').html("Add New product");
            $('#ajax-crud-modal').modal('show');
        });

        $('body').on('click', '#edit-product', function () {
            var product_id = $(this).data('id');
            $.get('product/'+product_id+'/edit', function (data) {
                $('#productCrudModal').html("Edit product");
                $('#btn-save').val("edit-product");
                $('#ajax-crud-modal').modal('show');
                $('#product_id').val(data.id);
                $('#category_id').val(data.category_id);
                $('#title').val(data.title);
                $('#slug').val(data.slug);  
                $('#description').val(data.description);  
                $('#status').val(data.status);  
                $('#featured_image_old').val(data.featured_image);  
            })
        });

        $('body').on('click', '.delete-product', function () {
            var product_id = $(this).data("id");
            if(confirm("Are You sure want to delete !")) {
                $.ajax({
                    type: "DELETE",
                    url: "{{ url('product')}}"+'/'+product_id,
                    success: function (data) {
                        $("#product_id_" + product_id).remove();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        });

        $('body').on('click', '#btn-save', function(e) {
            e.preventDefault();

            var actionType = $('#btn-save').val();
            $('#btn-save').html('Sending..');
            
            var form = $('#productForm')[0];
            var formData = new FormData(form);
            formData.append('featured_image', $('input[type=file]')[0].files[0]); 

            $.ajax({
                data: formData,
                url: "{{ route('product.store') }}",
                type: "post",
                processData: false,
                contentType: false,
                success: function (data) {
                    var product = '<tr id="product_id_' + data.id + '">';
                    product += '<td>' + data.id + '</td>';
                    product += '<td><img width="50" height="50" src="' + data.featured_image + '" alt="' + data.title + '" /></td>'    
                    product += '<td>' + data.title + '</td>';
                    product += '<td>' + data.slug + '</td>';
                    product += '<td>' + data.description + '</td>';
                    product += '<td>' + (data.status === 0 ? "Active" : "Inactive") + '</td>';
                    product += '<td><a href="javascript:void(0)" id="edit-product" data-id="' + data.id + '" class="btn btn-info">Edit</a></td>';
                    product += '<td><a href="javascript:void(0)" id="delete-product" data-id="' + data.id + '" class="btn btn-danger delete-product">Delete</a></td></tr>';
                    
                    
                    if (actionType == "create-product") {
                        $('#products-crud').prepend(product);
                    } else {
                        $("#product_id_" + data.id).replaceWith(product);
                    }

                    $('#productForm').trigger("reset");
                    $('#ajax-crud-modal').modal('hide');
                    $('#btn-save').html('Save Changes');
                    
                },
                error: function (data) {
                    console.log('Error:', data);
                    $('#btn-save').html('Save Changes');
                }
            });
        });

    });    
</script>
@endpush